import { Wall } from "../types/Wall";
import { areaFromWall } from "./areaFromWall";
import { totalAreaFromDoorAndWindow } from "./totalAreaFromDoorAndWindow";

export const totalAreaFromWall = ({width, height}: Wall, doorQuantity: number, windowQuantity: number) => {
    const totalArea = areaFromWall(width, height) - totalAreaFromDoorAndWindow(doorQuantity, windowQuantity);
    const totalAreaDecimalFormat = Math.round(totalArea * 100) / 100;
    return totalAreaDecimalFormat;
}

export const isWallHeight30CentimetersGreaterThanDoor = (wallHeight: number, doorQuantity: number) => {
    if (doorQuantity > 0) {
        return wallHeight >= 2.20;
    }
    return true;
}

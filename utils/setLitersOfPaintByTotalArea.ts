const PAINT_TIN_LITTLE = 0.5;
const PAINT_TIN_MEDDIUM = 2.5;
const PAINT_TIN_BIG = 3.6;
const PAINT_TIN_EXTRA_BIG = 18;

export const setLitersOfPaintByTotalArea = (totalArea: number) => {
    let litersOfPaint = totalArea / 5;
    let combinationOfLiters: Map<string, number> = new Map();
    if (litersOfPaint <= 1) return new Map().set('0.5', 2);
    return defineByLitersOfPaint(litersOfPaint, combinationOfLiters);
};

const defineByLitersOfPaint = (litersOfPaint: number, combinationOfLiters: Map<string, number>) => {
    if (litersOfPaint <= 0) return combinationOfLiters;

    if (Math.ceil(litersOfPaint) > PAINT_TIN_EXTRA_BIG) {
        let { quantityOfTin, newLitersOfPaint } = setQuantityOfTinAndLiters(litersOfPaint, PAINT_TIN_EXTRA_BIG);
        combinationOfLiters.set('18', quantityOfTin);
        return defineByLitersOfPaint(newLitersOfPaint, combinationOfLiters);
    }

    if (Math.ceil(litersOfPaint) > PAINT_TIN_BIG) {
        let { quantityOfTin, newLitersOfPaint } = setQuantityOfTinAndLiters(litersOfPaint, PAINT_TIN_BIG);
        combinationOfLiters.set('3.6', quantityOfTin);
        return defineByLitersOfPaint(newLitersOfPaint, combinationOfLiters);
    }

    if (Math.ceil(litersOfPaint) > PAINT_TIN_MEDDIUM) {
        let { quantityOfTin, newLitersOfPaint } = setQuantityOfTinAndLiters(litersOfPaint, PAINT_TIN_MEDDIUM);
        combinationOfLiters.set('2.5', quantityOfTin);
        return defineByLitersOfPaint(newLitersOfPaint, combinationOfLiters);
    }

    if (Math.ceil(litersOfPaint) > PAINT_TIN_LITTLE) {
        let { quantityOfTin, newLitersOfPaint } = setQuantityOfTinAndLiters(litersOfPaint, PAINT_TIN_LITTLE);
        combinationOfLiters.set('0.5', quantityOfTin);
        return defineByLitersOfPaint(newLitersOfPaint, combinationOfLiters);
    }    
}

const setQuantityOfTinAndLiters = (litersOfPaint: number, paintTinSize: number) => {
    let quantityOfTin = Math.floor(litersOfPaint / paintTinSize);
    if (quantityOfTin < 1) quantityOfTin = 1;
    let newLitersOfPaint = litersOfPaint - (quantityOfTin * paintTinSize);
    return { quantityOfTin, newLitersOfPaint };
}

import { Wall } from "../types/Wall";
import { areaFromWall } from "./areaFromWall";
import { totalAreaFromDoorAndWindow } from "./totalAreaFromDoorAndWindow";

const ACCEPTABLE_PERCENTAGE_OF_DOOR_AND_WINDOW = 50;

export const isDoorAndWindowAreaGreaterThan50PercentFromWallArea = (
    { width, height }: Wall, 
    doorQuantity: number, 
    windowQuantity: number
) => {
    const totalAreaFromDoorAndWindowByQuantity = totalAreaFromDoorAndWindow(doorQuantity, windowQuantity);
    const areaFromWallByWidthAndHeight = areaFromWall(width, height);
    return (totalAreaFromDoorAndWindowByQuantity * 100) / areaFromWallByWidthAndHeight > ACCEPTABLE_PERCENTAGE_OF_DOOR_AND_WINDOW;
}

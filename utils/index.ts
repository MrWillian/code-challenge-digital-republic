import { areaFromWall } from "./areaFromWall";
import { isDoorAndWindowAreaGreaterThan50PercentFromWallArea } from "./isDoorAndWindowAreaGreaterThan50PercentFromWallArea";
import { isWallHeight30CentimetersGreaterThanDoor } from "./isWallHeight30CentimetersGreaterThanDoor";
import { totalAreaFromDoorAndWindow } from "./totalAreaFromDoorAndWindow";
import { totalAreaFromWall } from "./totalAreaFromWall";
import { setLitersOfPaintByTotalArea } from "./setLitersOfPaintByTotalArea";
import { sanitizeCombinationsMapToArray } from "./sanitizeCombinationsMapToArray";

export {
    areaFromWall, 
    isDoorAndWindowAreaGreaterThan50PercentFromWallArea,
    isWallHeight30CentimetersGreaterThanDoor,
    totalAreaFromDoorAndWindow,
    totalAreaFromWall,
    setLitersOfPaintByTotalArea,
    sanitizeCombinationsMapToArray
};

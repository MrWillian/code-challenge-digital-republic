import { Combinations } from "../types/Combinations";

export const sanitizeCombinationsMapToArray = (combinations: Map<string, number>) => {
	let combinationsToDisplay: Combinations[] = [];
	combinations.forEach((value: number, key: string) => 
		combinationsToDisplay?.push({ paintTin: key, quantityOfTins: value})
	);
	return combinationsToDisplay;
}

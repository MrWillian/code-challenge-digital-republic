const DOOR_AREA = 0.8 * 1.9;
const WINDOW_AREA = 1.2 * 2;

export const totalAreaFromDoorAndWindow = (doorQuantity: number, windowQuantity: number) => {
    return (doorQuantity * DOOR_AREA) + (windowQuantity * WINDOW_AREA);
}

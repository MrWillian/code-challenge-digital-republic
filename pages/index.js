import Head from 'next/head';
import FormContainer from '../components/FormContainer';
import styles from '../styles/Home.module.css';

export default function Home() {
  return (
    <div className={styles.container}>
      <Head>
        <title>Custom Your Room</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <main className={styles.main}>
        <h1 className={styles.title}>
          Let's Custom Your Room!!
        </h1>

        <FormContainer />        
      </main>

      <footer className={styles.footer}>
        <a
          href="http://willianmarciel.vercel.app/"
          target="_blank"
          rel="noopener noreferrer"
        >
          Developed by{' '}
          <img src="/wm-logo.png" alt="WM Logo" className={styles.logo} />
          Willian Marciel
        </a>
      </footer>
    </div>
  )
}

import { Combinations } from "../../types/Combinations";
import { sanitizeCombinationsMapToArray, setLitersOfPaintByTotalArea } from "../../utils";

export const setCombinationsToShow = (totalAreaWallValues) => {
	let totalArea = totalAreaWallValues.reduce((accumulator, currentValue) => accumulator + currentValue, 0);
	let combinations = setLitersOfPaintByTotalArea(totalArea);
	let error: string = "";
	if (!combinations) {
		error = "Ocorreu algum erro ao tentar calcular";
	}
	let combinationsToDisplay: Combinations[] = sanitizeCombinationsMapToArray(combinations);
	return { error, combinationsToDisplay };
}

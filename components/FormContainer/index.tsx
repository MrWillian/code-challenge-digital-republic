import React, { useState } from "react";
import styles from './FormContainer.module.css';
import WallsContainer from '../WallsContainer';
import { Combinations } from "../../types/Combinations";
import { handleRules } from "./handleRules";
import { setCombinationsToShow } from "./setCombinationsToShow";

export default function FormContainer() {
	const [result, setResult] = useState<Combinations[]>([]);
	const [errors, setErrors] = useState<string[]>([]);

  	const handleSubmit = (event: React.FormEvent<HTMLFormElement>) => {
		event.preventDefault();
		setErrors([]);
		setResult([]);

		let { handledErrors, totalAreaWallValues } = handleRules(event);

		if (handledErrors.length === 0) {
			let { error, combinationsToDisplay } = setCombinationsToShow(totalAreaWallValues);
			handledErrors = [ error, ...handledErrors ];
			setResult(combinationsToDisplay);
		}

		setErrors(handledErrors);
  	}

  	return (
		<div className={styles.formContainer}>
			<form onSubmit={handleSubmit}>
				{errors ?
					errors?.map((error, index) => (
						<div className={styles.formError} key={index}>
							<p className={styles.error}>{error}</p>
						</div>
					))
					: <></>
				}
				<WallsContainer />
				<button type="submit">Calcular</button>
				{result.length !== 0 ?
					<div className={styles.resultContainer}>
						<p className={styles.resultTitle}>Você irá precisar de:</p>
						{result?.map((result, index) => (
							<span key={index} className={styles.resultText}>
								<span>{result.quantityOfTins}</span> lata(s) de <span>{result.paintTin} L</span>
							</span>
						))}
					</div>
					: 
					<div className={styles.formError}>
						<p className={styles.error}>Ocorreu algum erro...</p>
					</div>
				}
			</form>

			<div className={styles.image}>
				<img 
					src="https://images.unsplash.com/photo-1625585598750-3535fe40efb3?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=870&q=80" 
					alt="Room Image"
				/>
			</div>
		</div>
  	);
}

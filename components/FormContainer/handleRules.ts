import { 
    areaFromWall, 
    isDoorAndWindowAreaGreaterThan50PercentFromWallArea, 
    isWallHeight30CentimetersGreaterThanDoor, 
    totalAreaFromDoorAndWindow, 
    totalAreaFromWall 
} from "../../utils";

export const handleRules = (event) => {
	let wallValues: number[] = [];
	let totalAreaWallValues: number[] = [];
	let doorAndWindowValues: number[] = [];
	let handledErrors: string[] = [];

	for (let i = 0; i < 4; i++) {
		let wall = { 
			width: parseFloat(event.target.widhtWall[i].value), 
			height: parseFloat(event.target.heightWall[i].value) 
		};
		let doorQuantity = parseFloat(event.target.doorQuantityWall[i].value);
		let windowQuantity = parseFloat(event.target.windowQuantityWall[i].value);

		wallValues[i] = areaFromWall(wall.height, wall.width);
		totalAreaWallValues[i] = totalAreaFromWall(wall, doorQuantity, windowQuantity);
		doorAndWindowValues[i] = totalAreaFromDoorAndWindow(doorQuantity, windowQuantity);

		if (!isWallHeight30CentimetersGreaterThanDoor(wall.height, doorQuantity)) {
			handledErrors = [
				"A altura de paredes com porta deve ser, no mínimo, 30cm maior que a altura da porta!!", 
				...handledErrors
			];
		}

		if (isDoorAndWindowAreaGreaterThan50PercentFromWallArea(wall, doorQuantity, windowQuantity)) {
			handledErrors = [
				"O total de área das portas e janelas deve ser no máximo 50% da área de parede!!", ...handledErrors
			];
		}

		if (wallValues[i] < 1 || wallValues[i] > 50) {
			handledErrors = [
				"As paredes devem ter mais de 1m², e menos de 50m²!!", ...handledErrors
			];
		}
	}

	return { handledErrors, totalAreaWallValues };
}

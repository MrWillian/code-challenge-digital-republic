import React, { useState } from "react";
import styles from './Wall.module.css';

export default function Wall({ params }) {
    const [ wallWidth, setWallWidth ] = useState<string>("1");
    const [ wallHeight, setWallHeight ] = useState<string>("1");
    const [ doorQuantity, setDoorQuantity ] = useState<string>("0");
    const [ windowQuantity, setWindowQuantity ] = useState<string>("0");

    return (
        <>
            <h4 className={styles.wallTitle}>{params.name}</h4>
            <fieldset>
                <label htmlFor={`heightWall${params.id}`}>Altura(m):</label>
                <input 
                    type="text" 
                    id={`heightWall${params.id}`}
                    name={`heightWall`}
                    value={wallHeight}
                    onChange={(e) => setWallHeight(e.target.value)}
                />
                <label htmlFor={`widhtWall${params.id}`}>Largura(m):</label>
                <input 
                    type="text" 
                    id={`widhtWall${params.id}`}
                    name={`widhtWall`}
                    value={wallWidth}
                    onChange={(e) => setWallWidth(e.target.value)}
                />
            </fieldset>
            <fieldset>
                <label>Quantidade de Portas:</label>
                <input 
                    type="text" 
                    id={`doorQuantityWall${params.id}`}
                    name={`doorQuantityWall`}
                    value={doorQuantity}
                    onChange={(e) => setDoorQuantity(e.target.value)}
                />
            </fieldset>
            <fieldset>
                <label>Quantidade de Janelas:</label>
                <input 
                    type="text" 
                    id={`windowQuantityWall${params.id}`}
                    name={`windowQuantityWall`}
                    value={windowQuantity}
                    onChange={(e) => setWindowQuantity(e.target.value)} 
                />
            </fieldset>
        </>
    );
}

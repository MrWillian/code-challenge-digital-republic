import React from "react";
import styles from './WallsContainer.module.css';
import Wall from '../Wall';

const walls = [
    {
        id: 1,
        name: 'Parede 1'
    },
    {
        id: 2,
        name: 'Parede 2'
    },
    {
        id: 3,
        name: 'Parede 3'
    },
    {
        id: 4,
        name: 'Parede 4'
    }
];

export default function WallsContainer() {
    return (
        <div className={styles.wallsContainer}>
            {walls.map(wall => (
                <div className={styles.wall} key={wall.name}>
                    <Wall params={wall} />
                </div>
            ))}
        </div>
    );
}

import React from 'react';
import { render, screen } from '@testing-library/react';
import '@testing-library/jest-dom';
import Wall from '../../components/Wall';

describe('Wall', () => {
    it('renders component with params', () => {
        const params = { id: 1, name: 'Parede 1' };

        render(<Wall params={params} />);

        const wallTitle = screen.getByText('Parede 1');

        expect(wallTitle).toBeInTheDocument();
    });
});

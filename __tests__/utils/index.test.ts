import {
    areaFromWall,
    isDoorAndWindowAreaGreaterThan50PercentFromWallArea,
    isWallHeight30CentimetersGreaterThanDoor,
    totalAreaFromDoorAndWindow,
    totalAreaFromWall,
    setLitersOfPaintByTotalArea
} from '../../utils/';

describe('Utils', () => {
    it('should calculate the area from a Wall', () => {
        const wall = areaFromWall(5.1, 6.5);
        expect(wall).toBe(33.15);
    });

    it('should calculate the total area from a door and a window by the quantity of each', () => {
        const totalArea = totalAreaFromDoorAndWindow(1, 1);
        expect(totalArea).toBe(3.92);
    });

    it('should calculate the total area from a Wall', () => {
        const wall = {
            width: 5.1,
            height: 6.5
        };
        const totalArea = totalAreaFromWall(wall, 1, 1);
        expect(totalArea).toBe(29.23);
    });

    it('should calculate and check if the area from door(s) and window(s) is greater than 50% of Wall area', () => {
        const wall = { width: 5.1, height: 6.5 };
        const isGreatherThan50Percent = isDoorAndWindowAreaGreaterThan50PercentFromWallArea(wall, 1, 1);
        expect(isGreatherThan50Percent).toBeFalsy();
    });
    
    it('should be check if the wall height is 30 centimeters greater than the door - if it exists', () => {
        const result = isWallHeight30CentimetersGreaterThanDoor(2.2, 1);
        expect(result).toBeTruthy();
    });

    it('should be check if the combination of paint tin is correct', () => {
        const combinationOfLiters = setLitersOfPaintByTotalArea(48.26);
        let expectedCombinationOfLiters: Map<string, number> = new Map();
        expectedCombinationOfLiters.set('3.6', 2);
        expectedCombinationOfLiters.set('2.5', 1);
        expect(combinationOfLiters).toStrictEqual(expectedCombinationOfLiters);
    });

    it('should be check if the combination of paint tin is equal to the expected', () => {
        const combinationOfLiters = setLitersOfPaintByTotalArea(95);
        let expectedCombinationOfLiters: Map<string, number> = new Map();
        expectedCombinationOfLiters.set('18', 1);
        expectedCombinationOfLiters.set('0.5', 2);
        expect(combinationOfLiters).toStrictEqual(expectedCombinationOfLiters);
    });

    it('should be check if the combination of paint tin is correct and return false', () => {
        const combinationOfLiters = setLitersOfPaintByTotalArea(95);
        let expectedCombinationOfLiters: Map<string, number> = new Map();
        expectedCombinationOfLiters.set('18', 1);
        expectedCombinationOfLiters.set('0.5', 1);
        expect(combinationOfLiters).not.toStrictEqual(expectedCombinationOfLiters);
    });

    it('should be check if the necessary paint tin is less or equal than 1 liters of paint and return 2 tin of 0.5 liters', () => {
        const combinationOfLiters = setLitersOfPaintByTotalArea(4);
        let expectedCombinationOfLiters: Map<string, number> = new Map();
        expectedCombinationOfLiters.set('0.5', 2);
        expect(combinationOfLiters).toStrictEqual(expectedCombinationOfLiters);
    });

    it('should be check if the necessary paint tin is equal 1 liters of paint and return 2 tin of 0.5 liters', () => {
        const combinationOfLiters = setLitersOfPaintByTotalArea(5);
        let expectedCombinationOfLiters: Map<string, number> = new Map();
        expectedCombinationOfLiters.set('0.5', 2);
        expect(combinationOfLiters).toStrictEqual(expectedCombinationOfLiters);
    });
});

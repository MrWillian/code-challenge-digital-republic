export type Combinations = {
	paintTin: string,
	quantityOfTins: number
};
